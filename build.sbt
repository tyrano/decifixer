ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.1.3"

lazy val root = (project in file("."))
  .settings(
    name := "DeciFixer",
    idePackagePrefix := Some("eu.tyrano.decifix"),
    mainClass / assembly := file("eu.tyrano.decifix.main")
  )

libraryDependencies += "org.ow2.asm" % "asm" % "9.6"

