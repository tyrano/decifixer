package eu.tyrano.decifix

import org.objectweb.asm.*

import java.io.*
import java.util.zip.*
import scala.annotation.tailrec

@main
def main(in: String, out: String): Unit = {
  val deciFile = File(in)

  if (!deciFile.exists())
    throw FileNotFoundException("Unknown file: " + deciFile.getPath)


  val inputZipFile = ZipFile(deciFile)
  val zipInputStream = ZipInputStream(FileInputStream(deciFile))

  val outFile = File(out)
  val zipOutputStream = ZipOutputStream(FileOutputStream(out))

  @tailrec
  def findFile(zipEntry: ZipEntry, found: Boolean): Unit = {
    zipOutputStream.closeEntry()
    zipOutputStream.putNextEntry(ZipEntry(zipEntry.getName))

    val nowFound = if (zipEntry.getName.endsWith("deci/aE/a$z.class"))
      zipOutputStream.write(modifyBytes(inputZipFile.getInputStream(zipEntry).readAllBytes()))
      true
    else
      val buffer = new Array[Byte](1024)
      var bytesRead = zipInputStream.read(buffer)
      while (bytesRead != -1) {
        zipOutputStream.write(buffer, 0, bytesRead)
        bytesRead = zipInputStream.read(buffer)
      }
      found

    val entry = zipInputStream.getNextEntry
    if entry != null then findFile(entry, nowFound) else if nowFound then println("Modification finished") else throw FileNotFoundException("Cannot find deci.aE.a$z class in the provided decimation file")
  }

  findFile(zipInputStream.getNextEntry, false)

  zipInputStream.close()
  zipOutputStream.close()
}

def modifyBytes(bytes: Array[Byte]): Array[Byte] = {
  val classReader = ClassReader(bytes)
  val classWriter = ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES)
  val visitor = new ClassVisitor(Opcodes.ASM5, classWriter) {
    override def visitMethod(access: Int, name: String, descriptor: String, signature: String, exceptions: Array[String]): MethodVisitor = {
      val visitor = super.visitMethod(access, name, descriptor, signature, exceptions)
      if name.equals("fromBytes") && descriptor.equals("(Lio/netty/buffer/ByteBuf;)V") then CallBeforeReturn(Opcodes.ASM5, visitor) else visitor
    }
  }

  classReader.accept(visitor, ClassReader.EXPAND_FRAMES)
  classWriter.toByteArray
}

class CallBeforeReturn(access: Int, mv: MethodVisitor) extends MethodVisitor(access, mv) {
  override def visitInsn(opcode: Int): Unit = {
    if (opcode == Opcodes.RETURN) {
      mv.visitVarInsn(Opcodes.ALOAD, 0)
      mv.visitLdcInsn(java.lang.Long.MIN_VALUE)
      mv.visitFieldInsn(Opcodes.PUTFIELD, "deci/aE/a$z", "time", "J")
    }

    super.visitInsn(opcode)
  }
}